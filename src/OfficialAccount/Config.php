<?php

declare(strict_types=1);

namespace QianSionEasyWeChat\OfficialAccount;

class Config extends \QianSionEasyWeChat\Kernel\Config
{
    /**
     * @var array<string>
     */
    protected array $requiredKeys = [
        'app_id',
    ];
}
