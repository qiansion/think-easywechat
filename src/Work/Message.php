<?php

declare(strict_types=1);

namespace QianSionEasyWeChat\Work;

/**
 * @property string $Event
 * @property string $InfoType
 * @property string $MsgType
 * @property string $ChangeType
 */
class Message extends \QianSionEasyWeChat\Kernel\Message
{
    //
}
