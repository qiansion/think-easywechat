<?php

namespace QianSionEasyWeChat\Work;

use JetBrains\PhpStorm\Pure;

class Encryptor extends \QianSionEasyWeChat\Kernel\Encryptor
{
    #[Pure]
    public function __construct(string $corpId, string $token, string $aesKey)
    {
        parent::__construct($corpId, $token, $aesKey, null);
    }
}
