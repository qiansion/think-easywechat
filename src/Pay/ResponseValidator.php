<?php

declare(strict_types=1);

namespace QianSionEasyWeChat\Pay;

use QianSionEasyWeChat\Kernel\Exceptions\BadResponseException;
use QianSionEasyWeChat\Kernel\HttpClient\Response as HttpClientResponse;
use QianSionEasyWeChat\Pay\Contracts\Merchant as MerchantInterface;
use Psr\Http\Message\ResponseInterface as PsrResponse;

class ResponseValidator implements \QianSionEasyWeChat\Pay\Contracts\ResponseValidator
{
    public function __construct(protected MerchantInterface $merchant)
    {
    }

    /**
     * @throws \QianSionEasyWeChat\Kernel\Exceptions\BadResponseException
     * @throws \QianSionEasyWeChat\Kernel\Exceptions\InvalidConfigException
     * @throws \QianSionEasyWeChat\Pay\Exceptions\InvalidSignatureException
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function validate(PsrResponse|HttpClientResponse $response): void
    {
        if ($response instanceof HttpClientResponse) {
            $response = $response->toPsrResponse();
        }

        if ($response->getStatusCode() !== 200) {
            throw new BadResponseException('Request Failed');
        }

        (new Validator($this->merchant))->validate($response);
    }
}
