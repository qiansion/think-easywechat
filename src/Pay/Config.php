<?php

declare(strict_types=1);

namespace QianSionEasyWeChat\Pay;

class Config extends \QianSionEasyWeChat\Kernel\Config
{
    /**
     * @var array<string>
     */
    protected array $requiredKeys = [
        'mch_id',
        'secret_key',
        'private_key',
        'certificate',
    ];
}
