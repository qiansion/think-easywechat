<?php

declare(strict_types=1);

namespace QianSionEasyWeChat\Pay\Contracts;

use QianSionEasyWeChat\Kernel\HttpClient\Response;
use Psr\Http\Message\ResponseInterface;

interface ResponseValidator
{
    public function validate(ResponseInterface|Response $response): void;
}
