<?php

namespace QianSionEasyWeChat\Pay\Exceptions;

use QianSionEasyWeChat\Kernel\Exceptions\RuntimeException;

class InvalidSignatureException extends RuntimeException
{
}
