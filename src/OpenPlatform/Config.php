<?php

declare(strict_types=1);

namespace QianSionEasyWeChat\OpenPlatform;

class Config extends \QianSionEasyWeChat\Kernel\Config
{
    /**
     * @var array<string>
     */
    protected array $requiredKeys = [
        'app_id',
        'secret',
        'aes_key',
    ];
}
