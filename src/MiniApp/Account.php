<?php

declare(strict_types=1);

namespace QianSionEasyWeChat\MiniApp;

use QianSionEasyWeChat\MiniApp\Contracts\Account as AccountInterface;

class Account extends \QianSionEasyWeChat\OfficialAccount\Account implements AccountInterface
{
    //
}
