<?php

declare(strict_types=1);

namespace QianSionEasyWeChat\MiniApp\Contracts;

use QianSionEasyWeChat\Kernel\Contracts\AccessToken;
use QianSionEasyWeChat\Kernel\Contracts\Config;
use QianSionEasyWeChat\Kernel\Contracts\Server;
use QianSionEasyWeChat\Kernel\Encryptor;
use QianSionEasyWeChat\Kernel\HttpClient\AccessTokenAwareClient;
use Psr\Http\Message\ServerRequestInterface;
use Psr\SimpleCache\CacheInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

interface Application
{
    public function getAccount(): Account;

    public function getEncryptor(): Encryptor;

    public function getServer(): Server;

    public function getRequest(): ServerRequestInterface;

    public function getClient(): AccessTokenAwareClient;

    public function getHttpClient(): HttpClientInterface;

    public function getConfig(): Config;

    public function getAccessToken(): AccessToken;

    public function getCache(): CacheInterface;
}
