<?php

declare(strict_types=1);

namespace QianSionEasyWeChat\Kernel\Exceptions;

use Exception as BaseException;

class Exception extends BaseException
{
}
