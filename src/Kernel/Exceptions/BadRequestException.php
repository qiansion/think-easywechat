<?php

declare(strict_types=1);

namespace QianSionEasyWeChat\Kernel\Exceptions;

class BadRequestException extends Exception
{
}
