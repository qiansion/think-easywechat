<?php

declare(strict_types=1);

namespace QianSionEasyWeChat\Kernel\Contracts;

use QianSionEasyWeChat\Kernel\Contracts\AccessToken as AccessTokenInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

interface AccessTokenAwareHttpClient extends HttpClientInterface
{
    public function withAccessToken(AccessTokenInterface $accessToken): static;
}
