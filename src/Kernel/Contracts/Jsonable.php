<?php

declare(strict_types=1);

namespace QianSionEasyWeChat\Kernel\Contracts;

interface Jsonable
{
    public function toJson(): string|false;
}
